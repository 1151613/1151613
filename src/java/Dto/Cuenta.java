/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ACER
 */
@Entity
@Table(name = "Cuenta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cuenta.findAll", query = "SELECT c FROM Cuenta c"),
    @NamedQuery(name = "Cuenta.findByNroCuenta", query = "SELECT c FROM Cuenta c WHERE c.nroCuenta = :nroCuenta"),
    @NamedQuery(name = "Cuenta.findBySaldo", query = "SELECT c FROM Cuenta c WHERE c.saldo = :saldo"),
    @NamedQuery(name = "Cuenta.findByFechacreacion", query = "SELECT c FROM Cuenta c WHERE c.fechacreacion = :fechacreacion"),
    @NamedQuery(name = "Cuenta.findBySobreGiro", query = "SELECT c FROM Cuenta c WHERE c.sobreGiro = :sobreGiro")})
public class Cuenta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "nroCuenta")
    private Integer nroCuenta;
    @Basic(optional = false)
    @Column(name = "saldo")
    private double saldo;
    @Basic(optional = false)
    @Column(name = "fechacreacion")
    @Temporal(TemporalType.DATE)
    private Date fechacreacion;
    @Column(name = "sobreGiro")
    private Integer sobreGiro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nroCuenta")
    private List<Movimiento> movimientoList;
    @JoinColumn(name = "cedula", referencedColumnName = "cedula")
    @ManyToOne(optional = false)
    private Cliente cedula;
    @JoinColumn(name = "tipo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Tipo tipo;

    public Cuenta() {
    }

    public Cuenta(Integer nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public Cuenta(Integer nroCuenta, double saldo, Date fechacreacion) {
        this.nroCuenta = nroCuenta;
        this.saldo = saldo;
        this.fechacreacion = fechacreacion;
    }

    public Integer getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(Integer nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public Integer getSobreGiro() {
        return sobreGiro;
    }

    public void setSobreGiro(Integer sobreGiro) {
        this.sobreGiro = sobreGiro;
    }

    @XmlTransient
    public List<Movimiento> getMovimientoList() {
        return movimientoList;
    }
     public String ImprimirM(List<Movimiento> movimientotaList){
        int i=0; String s="";
        for (Movimiento object : movimientotaList) {
             s+="Fecha "+object.getFecha()+"Descripción "+object.getIdTipoMovimiento().getDescripcion()+ "\n";    
        }
          return  s;
    }
     
    public String ImprimirMovimientoF(List<Movimiento> movimientotaList,Date f1,Date f2){
        int i=0; String s="";
        for (Movimiento object : movimientotaList) {
            if(object.getFecha().after(f2) && object.getFecha().before(f1)){
             s+="Fecha "+object.getFecha()+"Descripción "+object.getIdTipoMovimiento().getDescripcion()+ "\n";  
            }  
        }
          return  s;
    }

    public void setMovimientoList(List<Movimiento> movimientoList) {
        this.movimientoList = movimientoList;
    }

    public Cliente getCedula() {
        return cedula;
    }

    public void setCedula(Cliente cedula) {
        this.cedula = cedula;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nroCuenta != null ? nroCuenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cuenta)) {
            return false;
        }
        Cuenta other = (Cuenta) object;
        if ((this.nroCuenta == null && other.nroCuenta != null) || (this.nroCuenta != null && !this.nroCuenta.equals(other.nroCuenta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Dto.Cuenta[ nroCuenta=" + nroCuenta + " ]";
    }
    
}
