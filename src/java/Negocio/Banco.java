/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;


import DAO.Conexion;
import Dao.ClienteJpaController;
import Dao.CuentaJpaController;
import Dao.MovimientoJpaController;
import Dao.TipoJpaController;
import Dao.TipoMovimientoJpaController;
import Dto.Cliente;
import Dto.Cuenta;
import Dto.Movimiento;
import Dto.Tipo;
import Dto.TipoMovimiento;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ACER
 */
public class Banco {

   // Conexion con = Conexion.getConexion();
    
    public Banco() {
    }
    

    public boolean insertarCliente(Integer cedula, String nombre, String dir, String fecha, String email, int telefono) {  // Integer cedula, String nombre, Date fechanacimiento, String dircorrespondencia, int telefono, String email
        Cliente nuevo = new Cliente();
        nuevo.setCedula(cedula);
        nuevo.setNombre(nombre);
        nuevo.setFechanacimiento(crearFecha(fecha));
        nuevo.setDircorrespondencia(dir);
        nuevo.setTelefono(telefono);
        nuevo.setEmail(email);
        Conexion con = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
        try {
            clienteDAO.create(nuevo);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
        return true;
    }

    public boolean eliminarCliente(Integer cedula) {
        Conexion con = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
        try {
            clienteDAO.destroy(cedula);
            return true;
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return false;
        }
    }

    public boolean editarCliente(Integer cedula, String nomEdicion, String dato) {
        boolean b = false;
        Conexion con = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
        Cliente nuevo = clienteDAO.findCliente(cedula);
        if (nomEdicion.equals("telefono")) {
            Integer telefono = Integer.parseInt(dato);
            try {
                nuevo.setTelefono(telefono);
                clienteDAO.edit(nuevo);
                b = true;
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                b = false;
            }
        } else {
            if (nomEdicion.equals("email")) {
                try {
                    nuevo.setEmail(dato);
                    clienteDAO.edit(nuevo);
                    b = true;
                } catch (Exception ex) {
                    System.err.println(ex.getMessage());
                    b = false;
                }
            } else {
                if (nomEdicion.equals("direccion")) {
                    try {
                        nuevo.setDircorrespondencia(dato);
                        clienteDAO.edit(nuevo);
                        b = true;
                    } catch (Exception ex) {
                        System.err.println(ex.getMessage());
                        System.out.println("no se pudo editar direccion");
                        b = false;
                    }
                } else {
                    if (nomEdicion.equals("nombre")) {
                        try {
                            nuevo.setNombre(dato);
                            clienteDAO.edit(nuevo);
                            b = true;
                        } catch (Exception ex) {
                            System.err.println(ex.getMessage());
                            b = false;
                        }
                    }
                }
            }
        }
        return b;
    }

    public boolean insertarCuenta(Integer nroCuenta, double saldo, String fechacreacion, Integer cedula, Integer tipoC) {
        Cuenta nuevaC = new Cuenta();
        Conexion con = Conexion.getConexion();
        nuevaC.setNroCuenta(nroCuenta);
        nuevaC.setSaldo(saldo);
        nuevaC.setFechacreacion(crearFecha(fechacreacion));
        TipoJpaController tipoDAO = new TipoJpaController(con.getBd());
        Tipo tipo = tipoDAO.findTipo(tipoC);
        //Ahora agrego su tipo y su cliente
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
        Cliente nuevo = clienteDAO.findCliente(cedula);
        nuevaC.setCedula(nuevo);
        nuevaC.setTipo(tipo);
        if(tipoC==2){
            nuevaC.setSobreGiro(1000000);
            }
        //Crear cuenta:   

        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
        try {
            cuentaDAO.create(nuevaC);
            return true;
        } catch (Exception ex) {
            //Logger.getLogger(TestConexion.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex.getMessage());
            return false;
        }
    }

    private Date crearFecha(String fecha) {
        Date f = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date parsed = format.parse(fecha);
            java.sql.Date g = new java.sql.Date(parsed.getTime());
            f = g;
            return f;
        } catch (ParseException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }
        return f;
    }

    public boolean eliminarCuenta(Integer numCuenta) {
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
        try {
            cuentaDAO.destroy(numCuenta);
            return true;
        } catch (Exception ex) {
            System.err.println(ex.getMessage());

            return false;
        }
    }

    public boolean editarDinero(Integer nroCuenta, Integer saldo) {
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
        Cuenta nueva = cuentaDAO.findCuenta(nroCuenta);
        try {
            nueva.setSaldo(saldo);
            cuentaDAO.edit(nueva);
            return true;
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return false;
        }
    }

    public boolean editarSobregiro(Integer nroCuenta, Integer saldo) {
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
        Cuenta nueva = cuentaDAO.findCuenta(nroCuenta);
        try {
            nueva.setSobreGiro(saldo);
            cuentaDAO.edit(nueva);
            return true;
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return false;
        }
    }

    public boolean tipoMovimiento(Integer id, Integer nroCuenta, Integer saldo, String fechacreacion, Integer destino) {
        //toma la cuenta
        Conexion con = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
        Cuenta x = cuentaDAO.findCuenta(nroCuenta);
      
        MovimientoJpaController movimientoDAO = new MovimientoJpaController(con.getBd());
        if (x != null) {
            if (id == 1) {
                return consignar2(x, saldo, fechacreacion, movimientoDAO);
            }

            if (id == 2) {
                return retirar2(x, saldo, fechacreacion, movimientoDAO);

            }

            if (id == 3) {
                CuentaJpaController cuentaDAOC = new CuentaJpaController(con.getBd());
                Cuenta d = cuentaDAOC.findCuenta(destino);
               boolean retirar= retirar2(x, saldo, fechacreacion, movimientoDAO);
               boolean consignar= consignar2(d, saldo, fechacreacion, movimientoDAO);
                if(retirar==true&&consignar==true){
                return true;
                }else return false;
            }
        } 
        return false;
    }

    private Movimiento consignar(Cuenta x, Integer saldo, String fechacreacion) {
        Movimiento nuevoM = new Movimiento();
        nuevoM.setFecha(crearFecha(fechacreacion));
        long nuevoSaldo = (long) (x.getSaldo() + saldo);
        x.setSaldo(nuevoSaldo);
        nuevoM.setNroCuenta(x);
        nuevoM.setValor((int) nuevoSaldo);
        editarDinero(x.getNroCuenta(), (int) nuevoSaldo);
        return nuevoM;
    }

    public boolean consignar2(Cuenta x, Integer saldo, String fechacreacion, MovimientoJpaController movimientoDAO) {
        Conexion con = Conexion.getConexion();
        TipoMovimientoJpaController tipoDAO = new TipoMovimientoJpaController(con.getBd());
        TipoMovimiento nuevo = tipoDAO.findTipoMovimiento(1);
        if (x.getTipo().getId() == 1) {
            Movimiento m = consignar(x, saldo, fechacreacion);
            m.setIdTipoMovimiento(nuevo);
            movimientoDAO.create(m);
            return true;
        } else {
            if (x.getTipo().getId() == 2) {
                Movimiento m = consignar(x, saldo, fechacreacion);
                m.setIdTipoMovimiento(nuevo);
                movimientoDAO.create(m);
                if (x.getSaldo() > 0) {
                    editarSobregiro(x.getNroCuenta(), 1000000);
                }
                return true;
            }
        }return false;
    }

    private Movimiento retirar(Cuenta x, Integer saldo, String fechacreacion) {
        Movimiento nuevoM = new Movimiento();
        nuevoM.setFecha(crearFecha(fechacreacion));
        long nuevoSaldo = (long) (x.getSaldo() - saldo);
        x.setSaldo(nuevoSaldo);
        nuevoM.setNroCuenta(x);
        nuevoM.setValor((int) nuevoSaldo);
        editarDinero(x.getNroCuenta(), (int) nuevoSaldo);
        return nuevoM;
    }

    public boolean retirar2(Cuenta x, Integer saldo, String fechacreacion, MovimientoJpaController movimientoDAO) {
        Conexion con = Conexion.getConexion();
        TipoMovimientoJpaController tipoDAO = new TipoMovimientoJpaController(con.getBd());
        TipoMovimiento nuevo = tipoDAO.findTipoMovimiento(2);
        if (x.getTipo().getId() == 1 && x.getSaldo() > 0) {
            Movimiento m = retirar(x, saldo, fechacreacion);
            m.setIdTipoMovimiento(nuevo);
            movimientoDAO.create(m);
            return true;
        } else {
            if (x.getTipo().getId() == 2 && x.getSaldo() > -1000000) {
                Movimiento m = retirar(x, saldo, fechacreacion);
                m.setIdTipoMovimiento(nuevo);
                movimientoDAO.create(m);
                if (x.getSaldo() < 0) {
                    editarSobregiro(x.getNroCuenta(),1000000);
                    Integer sa = (int) (x.getSaldo());;
                    editarSobregiro(x.getNroCuenta(),x.getSobreGiro()+sa);          
                }
                return true;
            }return false;
        }
    }
    
    public String informacionCliente(Integer cedula){
        Conexion con = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
        Cliente nuevo = clienteDAO.findCliente(cedula);
        String s = "" ;     
     //   CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
            s+=" El cliente es "+nuevo.getNombre()+ " de cédula "+ nuevo.getCedula()+" "+nuevo.ImprimirM(nuevo.getCuentaList());
         return s;
         
    }
    public String estratoBancario(Integer cedula,Date f1,Date f2){
        Conexion con = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(con.getBd());
        Cliente nuevo = clienteDAO.findCliente(cedula);
        String s = "" ;     
        CuentaJpaController cuentaDAO = new CuentaJpaController(con.getBd());
            s+=" El cliente es "+nuevo.getNombre()+ " de cédula "+ nuevo.getCedula()+" "+nuevo.ImprimirMF(nuevo.getCuentaList(), f1, f2);
         return s;
    }
    
       public void generar(Integer cedula,Date f1,Date f2) throws FileNotFoundException, DocumentException {       
        FileOutputStream archivo = new FileOutputStream(cedula + ".pdf");
        Document documento = new Document();
        PdfWriter.getInstance(documento, archivo);
        documento.open();
        
        Paragraph parrafo = new Paragraph("Informe Bancario");
        parrafo.setAlignment(1);
        documento.add(parrafo);
        
        documento.add(new Paragraph(estratoBancario( cedula ,f1, f2)));
        documento.close();
        JOptionPane.showMessageDialog(null, "Archivo PDF creado correctamente","Información",1);
        abrir(cedula);
        
    }
       public  void abrir (Integer cedula){
        try {
            File path = new File(cedula + ".pdf");
            Desktop.getDesktop().open(path);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex,"Atención",2);
        }
       }
    public static void main(String[] args) {
        //try {
            Banco b = new Banco();
            /* b.insertarCliente(1090,"carlos","1gdgd","1980-11-26","nada@nada.com",5555);
            b.insertarCuenta(12, 20000, "2019-02-02",100);
            b.editarCliente(600, "telefono", "313");
            b.editarCliente(600, "nombre", "Gabriela");
            b.editarCliente(600, "direccion", "Laureles");
            b.insertarCuenta(600, 50000, "2019-11-28",600,2);
            b.insertarCuenta(300, 50000, "2019-11-28",600,2);
            b.editarCliente(600, "email", "gabriela@ufps.edu.co");
            b.eliminarCuenta(12);
            */
            //String descripcion,Integer nroCuenta,Integer saldo,String fechacreacion
            // b.tipoMovimiento(3,300,4050, "2019-02-02",600);
            //b.insertarCuenta(851, 50000, "2019-11-28",600,2);
            //b.insertarCuenta(861, 50000, "2019-11-28",600,1);
      /*  b.generar(600, b.crearFecha("2019-02-0"), b.crearFecha("2019-02-0"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(Banco.class.getName()).log(Level.SEVERE, null, ex);
        }*/ //Integer id,Integer nroCuenta,Integer saldo,String fechacreacion,Integer destino
      //b.tipoMovimiento(2,851, 500000,"2019-12-03" , 0);
      b.tipoMovimiento(3, 851, 100000, "2019-12-03",861);
    }
}
